module.exports = {
  transform: {
    "^.+\\.ts$": "ts-jest"
  },
  roots: ["src/"],
  testEnvironment: "node",
  testRegex: "src(/.*)?/__tests__/[^/]*\\.test\\.(ts|js)$",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  collectCoverage: true,
  setupFilesAfterEnv: [],
  collectCoverageFrom: [
    "src/**/*.{ts,js}",
    "!src/**/*.d.ts",
    "!src/**/_tests_/**/*.*",
    "!src/test/**/*.*"
  ],
  coverageReporters: ["text-summary", "lcov", "html"],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100
    }
  }
};
