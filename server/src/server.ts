import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import dotEnvSafe from "dotenv-safe";
import cron from "node-cron";
import moment from "moment";

import routes from "./routes";
import { seekStories } from "./api/Stories/stories.controller";

dotEnvSafe.config();
mongoose.Promise = global.Promise;
let mongoUrl;

if (process.env.NODE_ENV === "test") {
  mongoUrl = `${process.env.MONGODB_URI}${process.env.DATABASE_NAME_TEST}`;
} else mongoUrl = `${process.env.MONGODB_URI}${process.env.DATABASE_NAME}`;

mongoose.connect(
  mongoUrl,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
  },
  err => {
    if (err) {
      console.log(err);
      console.log("Error en la conexion de BD");
    }
  }
);

const app = express();

app.use(express.json());

if (process.env.NODE_ENV !== "production") {
  app.use(cors());
}

app.use("/", routes);
app.set("env", process.env.NODE_ENV);
app.set("port", process.env.PORT || 4001);

app.listen(process.env.PORT, () => {
  console.log(`Started up at port ${process.env.PORT}`);
});

console.log(`Seeking stories at ${moment().format("LLLL")}`);
seekStories();

if (process.env.NODE_ENV !== "test") {
  cron.schedule("0 */1 * * *", () => {
    try {
      console.log(`Seeking stories at ${moment().format("LLLL")}`);
      seekStories();
    } catch (err) {
      console.log(`It was an error: ${err}`);
    }
  });
}

export { app };
