import axios from "axios";
import Story, { IStory } from "../../models/story";
import moment = require("moment");
import { uniqBy } from "lodash";

async function seekStories() {
  const api_url = process.env.HN_URL;
  axios.get(api_url).then(async data => {
    let { hits: stories } = data.data;
    stories = uniqBy(stories, "story_id");

    try {
      return stories.map(async (story: any) => {
        story.story_created_at = story.created_at;
        return await findOrCreateStory(story);
      });
    } catch (error) {
      console.log(error);
    }
  });
}

async function findOrCreateStory(story: any) {
  let newStory = new Story(story);
  await Story.findOne({ story_id: story.story_id }).exec((err, storyFound) => {
    if (storyFound === null) {
      console.log(`Creating story ${story.story_id}`);
      newStory.save(err => {
        if (err) console.log(`error saving the story:  ${err}`);
      });
    }
  });
}

const getAllStories = (req: any, res: any) => {
  Story.find({ isDeleted: false, deleted_at: null })
    .sort({ story_created_at: "desc" })
    .exec((err, stories: IStory[]) => {
      if (err) return res.status(500).send(err);

      return res.status(200).send({ stories });
    });
};

const deleteAStory = (req: any, res: any) => {
  const id = req.params.id;
  Story.findOneAndUpdate(
    { _id: id, isDeleted: false },
    { $set: { isDeleted: true, deleted_at: moment() } },
    { new: true }
  ).exec((err, story) => {
    if (err) {
      return res.status(500).send({ err });
    }
    return res.status(200).send({ story });
  });
};

export { getAllStories, seekStories, deleteAStory, findOrCreateStory };
