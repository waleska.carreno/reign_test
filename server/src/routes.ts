import express from "express";

import { getAllStories, deleteAStory } from "./api/Stories/stories.controller";

const routes = express.Router();
routes.route("/stories").get(getAllStories);
routes.route("/story/:id").delete(deleteAStory);

export default routes;
