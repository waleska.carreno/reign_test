import mongoose, { Schema, Document } from "mongoose";

export interface IStory extends Document {
  story_id: number;
  story_created_at: string;
  title: string;
  url: string;
  author: string;
  points: number;
  story_text: string;
  comment_text: string;
  num_comments: number;
  story_title: string;
  story_url: string;
  parent_id: number;
  created_at_i: string;
  _tags: string[];
  objectId: string;
  _highlightResult: any;
  isDeleted: boolean;
  deleted_at: any;
}

const StorySchema: Schema = new Schema(
  {
    story_id: { type: Number, required: true },
    story_created_at: { type: String, default: null },
    title: { type: String, default: null },
    url: { type: String, default: null },
    author: { type: String },
    points: { type: Number, default: null },
    story_text: { type: String, default: null },
    comment_text: { type: String, default: null },
    num_comments: { type: Number, default: null },
    story_title: { type: String, default: null },
    story_url: { type: String, default: null },
    parent_id: { type: Number },
    created_at_i: { type: String },
    _tags: { type: Array },
    objectId: { type: String },
    _highlightResult: { type: Object },
    isDeleted: { type: Boolean, default: false },
    deleted_at: { type: Date, default: null }
  },
  {
    timestamps: true
  }
);

const Story = mongoose.model<IStory>("Story", StorySchema);
export default Story;
