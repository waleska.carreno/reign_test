import mongoose from "mongoose";
import request from "supertest";
import { app } from "../server";
import {
  findOrCreateStory,
  seekStories,
  getAllStories
} from "../api/Stories/stories.controller";
import Story from "../models/story";

const databaseName = "test";

const story = {
  story_created_at: "2020-02-16T18:46:38.000Z",
  title: "",
  url: "",
  points: "",
  story_text: "",
  comment_text:
    '&gt; Parsing, printing, and source map generation are all fully parallelized<p>I think this the major cause of speedup. Now that NodeJS supports WorkerThreads, it can be done in parallel. Along with this if right choice of data structures used there is no need to depend upon writing a build manager in another language instead of host language.<p>Also, as of today JS(V8) performance is getting closer to Go runtime.<p><a href="https:&#x2F;&#x2F;benchmarksgame-team.pages.debian.net&#x2F;benchmarksgame&#x2F;fastest&#x2F;go-node.html" rel="nofollow">https:&#x2F;&#x2F;benchmarksgame-team.pages.debian.net&#x2F;benchmarksgame&#x2F;...</a>',
  num_comments: "",
  story_title: "ESbuild – A fast JavaScript bundler and minifier in Go",
  story_url: "https://github.com/evanw/esbuild/",
  _tags: ["comment", "author_truth_seeker", "story_22335707"],
  isDeleted: false,
  deleted_at: "",
  author: "truth_seeker",
  story_id: 22335707,
  parent_id: 22335707,
  created_at_i: "1581878798",
  _highlightResult: {}
};

describe("Story test", () => {
  beforeAll(async () => {
    const newStory = new Story(story);
    await newStory.save();
  });
  it("should create a new document when you exec find or create function with an unexisting story_id", async () => {
    story.story_id = 123333;
    findOrCreateStory(story);
    const result = await Story.findOne({ story_id: story.story_id });
    expect(result).toHaveProperty(["_id"]);
  });
  it("should return all stories saved in the database", () => {
    request(app)
      .get("/stories")
      .expect(200)
      .expect(res => res.body.data.toBeTruthy());
  });
  afterAll(() => {
    for (var collection in mongoose.connection.collections) {
      mongoose.connection.collections[collection].drop(() => {});
    }
  });
});
