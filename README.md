# Reign Test Repository

Whole project for Reign Test.

This repository is composed by two projects.

- Server (backend API)
- Client (FrontEnd App)

* [Requirements](#requirements)
* [Installation](#installation)
* [Testing](#testing)

## Requirements

- [Node.js v10.x LTS](https://nodejs.org/en/)
- [Docker](http://docker.com)

## Installation

- Clone this repository in your local.
- Configure your own .env file in both project (you just copy all env from .env.example).
- Run your local docker service
- The repository has a docker-compose file. In your local terminal, go to the docker-compose's directory and run `docker-compose up -build`
- Open the client in your browser like `localhost:3000`

## Testing

In the server, tests run independently.
Go to server's directory and run:

- `npm run test`
