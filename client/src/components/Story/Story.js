import React, { useState } from "react";
import { Typography, Container, Box, IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { get } from "lodash";
import moment from "moment";

import "./Story.css";

const Story = props => {
  const { story, deleteStory } = props;
  const [isHover, setIsHover] = useState(false);

  return (
    <Container
      maxWidth="xl"
      className={isHover ? "isHover" : ""}
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
    >
      <Box
        display="flex"
        flexDirection="row"
        justifyContent="space-between"
        className="story-container"
        alignItems="center"
      >
        <a
          href={
            get(story, "url") !== null
              ? get(story, "url")
              : get(story, "story_url")
          }
          style={{ width: "70%" }}
        >
          <Box
            display="flex"
            justifyContent="flex-start"
            alignItems="center"
            style={{ width: "70%" }}
            fontSize="13px"
          >
            <Typography variant="subtitle1" align="left">
              <Box fontSize="13px" color="#333">
                {get(story, "title")
                  ? get(story, "title")
                  : get(story, "story_title")}
              </Box>
            </Typography>
            <Typography variant="subtitle1" align="left">
              <Box fontSize="13px" color="#999" m={2}>
                - {get(story, "author")} -
              </Box>
            </Typography>
          </Box>
        </a>
        <Box
          display="flex"
          justifyContent="space-around"
          style={{ width: "20%" }}
          alignItems="center"
        >
          <Typography variant="subtitle1" m={4}>
            <Box fontSize="13px">
              {moment(get(story, "story_created_at")).format("LLL")}
            </Box>
          </Typography>
          <IconButton
            aria-label="delete"
            onClick={() => {
              console.log(story._id);
              deleteStory(story._id);
            }}
          >
            <DeleteIcon />
          </IconButton>
        </Box>
      </Box>
    </Container>
  );
};

export default Story;
