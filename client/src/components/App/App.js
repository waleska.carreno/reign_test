import React from "react";
import useAxios from "axios-hooks";
import { AppBar, Typography, Box } from "@material-ui/core";
import axios from "axios";
import { get } from "lodash";

import Story from "../Story/Story";
import "./App.css";

const App = () => {
  const [{ data: getData }, executeGet] = useAxios(
    `${process.env.REACT_APP_API_URL}/stories`,
    {
      headers: { "Access-Control-Allow-Origin": "*" }
    }
  );

  const deleteStory = id => {
    axios.delete(`${process.env.REACT_APP_API_URL}/story/${id}`).then(res => {
      executeGet();
    });
  };
  console.log(process.env.REACT_APP_API_URL);

  return (
    <div className="App">
      <header className="App-header">
        <AppBar position="static" color="transparent">
          <div className="header-title">
            <Typography variant="h1" align="left">
              <Box fontWeight="fontWeightBold">HN Feed</Box>
            </Typography>
            <Typography variant="h5" align="left">
              <Box fontWeight="fontWeightLight">We &lt;3 hacker news</Box>
            </Typography>
          </div>
        </AppBar>
      </header>
      {getData &&
        getData.stories.map((story, key) => {
          if (story.title != null || story.story_title != null) {
            return <Story story={story} deleteStory={deleteStory} />;
          }
        })}
      {(!getData || get(getData, "stories.length") === 0) && (
        <Box
          display="flex"
          justifyContent="center"
          padding="30px"
          borderBottom="1px solid #fafafa"
        >
          <Typography variant="h6" align="left">
            <Box fontWeight="fontWeightLight">No stories</Box>
          </Typography>
        </Box>
      )}
    </div>
  );
};

export default App;
